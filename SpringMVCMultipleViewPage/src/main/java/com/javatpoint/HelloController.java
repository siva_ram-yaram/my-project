package com.javatpoint;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
	@RequestMapping("/hello")
	public String redirect1() {
		return "viewpage";
	}

	@RequestMapping("/helloagain")
	public String display() {
		return "final";
	}

	@RequestMapping("/hello")
	public String redirect() {
		return "viewpage";
	}

	@RequestMapping("/helloagain")
	public String display1() {
		return "final";
	}
}
